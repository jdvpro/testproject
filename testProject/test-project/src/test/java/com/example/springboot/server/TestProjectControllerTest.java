package com.example.springboot.server;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectControllerTest {

	@Mock
	private TestProjectRepository repository;
	@InjectMocks
	private TestProjectController controller;

	@Test
	public void testGetInfo() {
		List<TestProjectInfo> list = new ArrayList<>(3);
		list.add(new TestProjectInfo(5l, "mess5"));
		list.add(new TestProjectInfo(4l, "mess4"));

		List<Long> collection = Arrays.asList(5l, 4l);
		when(repository.findByIdIn(collection)).thenReturn(list);
		Assert.assertEquals(list, controller.getInfo(collection));

		list.add(new TestProjectInfo(2l, "mess2"));
		when(repository.findAll()).thenReturn(list);
		Assert.assertEquals(list, controller.getInfo(null));

	}

	@Test
	public void testTransform() {
		Map<Long, String> map = new HashMap<>();
		map.put(3l, "Hello, 3! 3*2 = 6.");
		map.put(5l, "Hello, 5! 5*2 = 10.");

		List<Long> list = Arrays.asList(5l, 3l);
		Assert.assertEquals(map, controller.transform(list));
	}

	// second method for test because of multithreading
	@Test
	public void testTransform2() {
		Map<Long, String> map = new HashMap<>();
		map.put(3l, "Hello, 3! 3*2 = 6.Hello, 5! 5*2 = 10.");
		map.put(5l, "Hello, 5! 5*2 = 10.");

		List<Long> list = Arrays.asList(5l, 3l, 5l);
		Assert.assertEquals(map, controller.transform(list));
	}

	@Test
	public void testTransform3() {
		Map<Long, String> map = new HashMap<>();
		map.put(3l, "Hello, 3! 3*2 = 6.Hello, 5! 5*2 = 10.");
		map.put(5l, "Hello, 5! 5*2 = 10.");

		List<Long> list = Arrays.asList(5l, 3l, 5l);
		Assert.assertEquals(map, controller.transform(list));
	}

}
