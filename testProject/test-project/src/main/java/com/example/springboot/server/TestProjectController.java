package com.example.springboot.server;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class TestProjectController {

	@Autowired
	private TestProjectRepository repository;
	private final TestProjectService service = new TestProjectService();

	public TestProjectController() {
		log.info("***");
	}

	@RequestMapping("/")
	public String index() {
		return "Greetings!";
	}

	@RequestMapping("/info")
	public List<TestProjectInfo> getInfo(@RequestParam(required = false) final List<Long> args) {
		log.info("> getInfo args:" + args);

		if (CollectionUtils.isEmpty(args)) { return repository.findAll(); }

		return repository.findByIdIn(args);
	}

	@RequestMapping(value = "/transform", method = RequestMethod.POST)
	public Map<Long, String> transform(@RequestParam final List<Long> args) {
		log.info("> transform args:" + args);
		long start = System.currentTimeMillis();

		ExecutorService pool = Executors.newFixedThreadPool(2);
		Map<Long, String> map = new HashMap<>();
		List<Long> list = args.stream().distinct().collect(Collectors.toList());
		for (Long item : list) {
			Future<String> m1Future = pool.submit(() -> service.method1(item));
			Future<String> m2Future = pool.submit(() -> service.method2(item));
			try {
				String m1 = m1Future.get();
				String m2 = m2Future.get();

				String result = service.combine(m1, m2);
				map.put(item, result);

				save(item, result);

			} catch (InterruptedException | ExecutionException ex) {
				ex.printStackTrace();
			}
		}
		pool.shutdown();

		StringBuilder sb = new StringBuilder();
		listMatching(args).forEach(item -> {
			sb.append(map.get(item));
		});
		map.put(list.get(list.size() - 1), map.get(list.get(list.size() - 1)) + sb.toString());

		log.info(" transform time: " + (System.currentTimeMillis() - start));
		log.info("< map: " + map);
		return map;
	}

	private synchronized void save(final Long item, final String result) {
		if (!repository.findById(item).isPresent()) {
			repository.save(new TestProjectInfo(item, result));
		}
	}

	private synchronized Set<Long> listMatching(final List<Long> allStrings) {
		Set<Long> allItems = new HashSet<>();
		Set<Long> duplicates = allStrings.stream().filter(string -> !allItems.add(string)).collect(Collectors.toSet());

		return duplicates;
	}

	public ExecutorService executor = Executors.newFixedThreadPool(20);

	@RequestMapping(value = "/fasttransform", method = RequestMethod.POST)
	public Map<Long, String> process(@RequestParam final List<Long> ids) {
		long start = System.currentTimeMillis();
		Map<Long, String> result = ids.stream().parallel().collect(Collectors.toConcurrentMap(k -> k, v -> processId(v),
				(v1, v2) -> v1 + v2));

		log.info(" transform3 time: " + (System.currentTimeMillis() - start));
		return result;
	}

	public String processId(final Long id) {
		Future<String> r1 = executor.submit(() -> service.method1(id));
		Future<String> r2 = executor.submit(() -> service.method2(id));

		try {
			return service.combine(r1.get(), r2.get());
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	@RequestMapping(value = "/betatransform", method = RequestMethod.POST)
	public Map<Long, String> transform2(@RequestParam final List<Long> args) throws InterruptedException,
			ExecutionException {
		long start = System.currentTimeMillis();

		List<CompletableFuture<TestProjectInfo>> futures = args.stream().distinct().map(arg -> execute2(arg)).collect(
				Collectors.toList());

		CompletableFuture<Void> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures
				.size()]));
		CompletableFuture<List<TestProjectInfo>> allTestProjectInfo = allFutures.thenApply(v -> {
			return futures.stream().map(future -> future.join()).collect(Collectors.toList());
		});

		final Map<Long, String> map = allTestProjectInfo.get().stream().collect(Collectors.toMap(i -> i.getId(), i -> i
				.getMessage()));

		StringBuilder sb = new StringBuilder();
		listMatching(args).forEach(item -> {
			sb.append(map.get(item));
		});
		map.put(args.get(args.size() - 1), map.get(args.get(args.size() - 1)) + sb.toString());

		log.info(" transform3 time: " + (System.currentTimeMillis() - start));
		log.info("< map: " + map);
		return map;
	}

	private CompletableFuture<TestProjectInfo> execute2(final Long item) {
		Future<String> r1 = executor.submit(() -> service.method1(item));
		Future<String> r2 = executor.submit(() -> service.method2(item));

		return CompletableFuture.supplyAsync(() -> {
			try {
				return new TestProjectInfo(item, service.combine(r1.get(), r2.get()));
			} catch (InterruptedException | ExecutionException e) {
				throw new RuntimeException();
			}
		});
	}

}
