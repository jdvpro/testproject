package com.example.springboot.server;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestProjectService {

	public TestProjectService() {
		log.info("***");
	}

	public String method1(final Long value) {
		// emulate heavy remote call
		try {
			Thread.sleep(2000);
			// Thread.sleep(Double.valueOf(Math.random() * 2000).longValue());
		} catch (InterruptedException e) {
			// ignore
		}
		return "Hello, " + value + "!";
	}

	public String method2(final Long value) {
		// emulate heavy remote call
		try {
			Thread.sleep(2000);
			// Thread.sleep(Double.valueOf(Math.random() * 2000).longValue());
		} catch (InterruptedException e) {
			// ignore
		}
		return value + "*2 = " + (value * 2) + ".";
	}

	public String combine(final String s1, final String s2) {
		return s1 + " " + s2;
	}
}
