package com.example.springboot.server;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class TestProjectInfo {

	@Id
	private long id;

	private String message;

	@Temporal(value = TemporalType.TIMESTAMP)
	private Date date;

	public TestProjectInfo(final Long id, final String message) {
		this.id = id;
		this.message = message;
		this.date = new Date();
	}

}
