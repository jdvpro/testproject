package com.example.springboot.server;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestProjectRepository extends JpaRepository<TestProjectInfo, Long> {

	List<TestProjectInfo> findByIdIn(List<Long> ids);
}
